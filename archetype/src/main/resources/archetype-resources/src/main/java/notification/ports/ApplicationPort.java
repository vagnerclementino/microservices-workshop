package ${package}.notification.ports;

import ${package}.notification.domains.Notification;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

public interface ApplicationPort {

    void notify(@Valid @NotNull Notification notification);
}
