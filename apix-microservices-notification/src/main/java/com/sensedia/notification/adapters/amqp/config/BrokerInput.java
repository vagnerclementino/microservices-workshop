package com.sensedia.notification.adapters.amqp.config;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.SubscribableChannel;

public interface BrokerInput {

  @Input(BindConfig.SUBSCRIBE_ACCOUNT_CREATED)
  SubscribableChannel subscribeAccountCreated();
}
